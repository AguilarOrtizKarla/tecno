<!DOCTYPE html>
<html lang="es">
<!-- AUTOR: http://goo.gl/FYFUc0 -->
<!-- Validación Ingreso formulario x Lado Servidor -->
<!-- Validacción Ingreso del Cliente JavaScript -->
<!-- Actualizado: 2014.12.14 Por: Rusel Cierto Trinidad -->
	
<head>
	<meta charset="UTF-8">
	<title>Busqueda de Establecimiento | Paul Beltrand </title>
	<link type="text/css" rel="stylesheet" href="estilo.csscss">
    
</head>
<body>
<h1>__:: Busqueda de estableciemiento::__</h1>
<form  name="form1" method="Post" action="ejemplo.php">
<!-- form1=nombre del formulario, acción=validación del formulario -->
	<fieldset>
	<legend>Datos de Ingreso</legend>
	<div class="<?php echo $claseNombre; ?>" >
		<label>Encontar :</label>
		<input type="text" name="encontar" value="<?php echo $encontar; ?>" >
		<span class="msg"><?php echo $msgNombre; ?></span>
	</div>
	<!-- <br> -->
	<div class="<?php echo $claseEdad; ?>" >
		<label>Cerca :</label>
		<input type="text" name="cerca" value="<?php echo $cerca; ?>">

		<span class="msg"><?php echo $msgEdad; ?></span>
	</div>
	<div>
		<input type="submit" value="Buscar">
	</div>
	</fieldset>
</form>
	
<h1>Establecimientos</h1>
<div id="id01"></div>



<script src="http://maps.google.com/maps/api/js?sensor=false&callback=iniciar">
</script>

<script>
function iniciar(a,b,c) {
var mapOptions = {
center: new google.maps.LatLng(a,b),
zoom: 15,
mapTypeId: google.maps.MapTypeId.ROADMAP};
var map = new google.maps.Map(document.getElementById("map"),mapOptions);

    var marker2 = new google.maps.Marker({
        position: map.getCenter()
        , title: 'Establecimiento buscado'
        , map: map
        , });
var popup = new google.maps.InfoWindow({
        content: c }
                                      );
        popup.open(map, marker2);

}  

    
</script>

<div id="map" style="height: 200px; width: 50%; position: relative; background-color: rgb(229, 227, 223); overflow: hidden; transform: translateZ(0px) translateZ(0px) translateZ(0px) translateZ(0px) translateZ(0px);"></div>


    
</body>
    
    
<?php

	
	 
	 
@ob_end_clean();
/**
 * Yelp API v2.0 code sample.
 *
 * This program demonstrates the capability of the Yelp API version 2.0
 * by using the Search API to query for businesses by a search term and location,
 * and the Business API to query additional information about the top result
 * from the search query.
 * 
 * Please refer to http://www.yelp.com/developers/documentation for the API documentation.
 * 
 * This program requires a PHP OAuth2 library, which is included in this branch and can be
 * found here:
 *      http://oauth.googlecode.com/svn/code/php/
 * 
 * Sample usage of the program:
 * `php sample.php --term="bars" --location="San Francisco, CA"`
 */

// Enter the path that the oauth library is in relation to the php file
require_once('lib/OAuth.php');

// Set your OAuth credentials here  
// These credentials can be obtained from the 'Manage API Access' page in the
// developers documentation (http://www.yelp.com/developers)
$CONSUMER_KEY = "eSZxLiokqffxkHKdCxF_oQ";
$CONSUMER_SECRET = "qicXfqI_FqB3fFOxRPxxREobvos";
$TOKEN = "cyPsvmPh20xbkXFpAkHeoPFq0Osty800";
$TOKEN_SECRET = "lgfoxu2xx6X_LsO4jY9J7PjfMKE";



$API_HOST = 'api.yelp.com';
$DEFAULT_TERM = $_POST["encontar"];
$DEFAULT_LANG = 'es';
$DEFAULT_CC = 'MX';
$DEFAULT_LOCATION = $_POST["cerca"];
$SEARCH_LIMIT = 3;
$SEARCH_PATH = '/v2/search/';
$BUSINESS_PATH = '/v2/business/';


/** 
 * Makes a request to the Yelp API and returns the response
 * 
 * @param    $host    The domain host of the API 
 * @param    $path    The path of the APi after the domain
 * @return   The JSON response from the request      
 */
function request($host, $path) {
    $unsigned_url = "https://" . $host . $path;

    print_r("$unsigned_url\n");

    // Token object built using the OAuth library
    $token = new OAuthToken($GLOBALS['TOKEN'], $GLOBALS['TOKEN_SECRET']);

    echo $GLOBALS['TOKEN'];
    // Consumer object built using the OAuth library
    $consumer = new OAuthConsumer($GLOBALS['CONSUMER_KEY'], $GLOBALS['CONSUMER_SECRET']);

    // Yelp uses HMAC SHA1 encoding
    $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

    $oauthrequest = OAuthRequest::from_consumer_and_token(
        $consumer, 
        $token, 
        'GET', 
        $unsigned_url
    );
    
    // Sign the request
    $oauthrequest->sign_request($signature_method, $consumer, $token);
    
    // Get the signed URL
    $signed_url = $oauthrequest->to_url();
    
    // Send Yelp API Call
    $ch = curl_init($signed_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);

     // In case you're running this against a server w/o
        //   properly signed certs
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    $data = curl_exec($ch);
    curl_close($ch);
   // echo $data.'daaata';

    return $data;
}

/**
 * Consulta la API de búsqueda por un término de búsqueda y localización
 * 
 * @param    $term        The search term passed to the API 
 * @param    $location    The search location passed to the API 
 * @return   The JSON response from the request 
 */
// creacion de la url, peticion a la api
function search($term, $location) {
    $url_params = array();
    
    $url_params['term'] = $term ?: $GLOBALS['DEFAULT_TERM'];
    $url_params['location'] = $location?: $GLOBALS['DEFAULT_LOCATION'];
    $url_params['limit'] = $GLOBALS['SEARCH_LIMIT'];
    $url_params['cc'] = $GLOBALS['DEFAULT_CC'];
    $url_params['lang'] = $GLOBALS['DEFAULT_LANG'];

    //print_r($url_params);
    $search_path = $GLOBALS['SEARCH_PATH'] . "?" . http_build_query($url_params);
    
    //print("$search_path");
    return request($GLOBALS['API_HOST'], $search_path);
}

/**
 *Consulta la API de Business por business_id
 * 
 * @param    $business_id    El ID de la empresa para consultar
 * @return   La respuesta JSON de la solicitud
 */
function get_business($business_id) {
    $business_path = $GLOBALS['BUSINESS_PATH'] . $business_id;
    
    return request($GLOBALS['API_HOST'], $business_path);
}

/**
 * Consulta el API por los valores de entrada desde el usuario
 * 
 * @param    $term        The search term to query
 * @param    $location    The location of the business to query
 */
function query_api($term, $location) { 
       
    $response = json_decode(search($term, $location));
    
    $business_id = $response->businesses[0]->id;
    
    print sprintf(
        "%d empresas encontraron, la consulta de información de negocio para el primer resultado \"%s\"\n\n",         
        count($response->businesses),
        $business_id
    );
    
    $response = get_business($business_id);
    
    print sprintf("El resultado para los negocios \"%s\" found:\n", $business_id);
    print "$response\n";
    
  

}

/**
 * La entrada del usuario se maneja aquí
 */
$longopts  = array(
    "term::",
    "location::",
);
    
$options = getopt("", $longopts);

$term = $options['term'] ?: '';
$location = $options['location'] ?: '';

$res = json_decode(search($term, $location));

//print_r($res->businesses);

echo "<html><body><table border=1>";
echo "<tr><th>Nombre</th><th>Direccion</th><th>Telefono</th><th>Imagen</th><th>Ver</th></th>";



foreach ($res->businesses as $row) {
    echo "<tr><td>".$row->name."</td><td>".$row->location->display_address[0].$row->location->display_address[1]."</td><td>".$row->phone."</td><td>"."<img src=\"".$row->image_url."\"></td><td><input type=\"submit\" value=\"Mapa\" onclick=\"iniciar(".$row->location->coordinate->latitude.",".$row->location->coordinate->longitude.",'".$row->name."')\" name=\"Botonmap\"/></td>";
}
echo "</table></body></html>";


	?>
    
    
</html>

